class Item:
    def __init__(self, name, price, product_id, description, rating, categories):
        self.name = name
        self.price = price
        self.product_id = product_id
        self.description = description
        self.rating = rating
        self.categories = categories


class Movie(Item):
    def __init__(self, age_rating, name, price, product_id, description, rating, categories):
        super().__init__(name, price, product_id, description, rating, categories)
        self.age_rating = age_rating


class Book(Item):
    def __init__(self, author, isbn, name, price, product_id, description, rating, categories):
        super().__init__(name, price, product_id, description, rating, categories)
        self.author = author
        self.isbn = isbn


if __name__ == "__main__":
    test1 = Book("auth","123122","nume","14","221113","Is good","9/10",["action","comedy"])
    test2 = Movie("t","nume","14","221113","Is good","9/10",["action","comedy"])