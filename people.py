class Person:
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return "Person({})".format(self.name)

class Customer(Person):
    cart = {} #product and quantity

    def __init__(self, name, email, phone_number):
        super().__init__(name)
        self.email = email
        self.phone_number = phone_number
        self.money = 0
        
    def __repr__(self):
        return "Person({})".format(self.name)

class Reviewer(Customer):
    def __init__(self, name, email, phone_number, comments=None):
        super().__init__(name, email, phone_number)
        self.comments = comments

    def __repr__(self):
        return "Person({})".format(self.name)


if __name__ == "__main__":
	test1 = Customer("ion","ion@ion.ion","1312313123")
	test2 = Reviewer("Vasile","asdds@asd.com","1312312",["ok", "good"])